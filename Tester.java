class Shape {
    public double area() {
        return 0;
    }

    public double perimeter() {
        return 0;
    }
}

class TwoDimensionalShape extends Shape {
    
    public double area() {
        return 0;
    }

    public double perimeter() {
        return 0;
    }

}

class ThreeDimensionalShape extends Shape {
    public double area() {
        return 0;
    }

    public double volume() {
        return 0;
    }
}

class Circle extends TwoDimensionalShape {
   
    double radius;

    public Circle(double r) {
        radius = r;
    }
    
    public double getRadius() {
        return radius;
    }

    public double area() {
        return 3.14159 * radius * radius;
    }

    public double perimeter() {
        return 2 * 3.14159 * radius;
    }

}

class Sphere extends ThreeDimensionalShape {
    
    Circle circle;

    public Sphere(double r) {
        this.circle = new Circle(r);
    }
    public double area() {
        return 4 * circle.area();
    }
    public double volume() {
        return (4.0/3.0)*circle.area()*circle.getRadius();
    }
}

class Square extends TwoDimensionalShape {
    double side;
    public Square(double s) {
        side = s;
    }
    public double area() {
        return side*side;
    }
    public double perimeter() {
        return 4 * side;
    }
}

class Cube extends ThreeDimensionalShape {
    Square sq;
    public Cube(double s) {
        this.sq = new Square(s); // what this command do, I have no idea!
    }
    public double area() {
        return 6 * sq.area();
    }
    public double volume() {
        return sq.area() * sq.perimeter() / 4.0; // it will give s^3
    }
}

class Triangle extends TwoDimensionalShape {
    double side1, side2, side3, height;
    public Triangle(double s1, double s2, double s3, double h) {
        side1 = s1;
        side2 = s2;
        side3 = s3;
        height = h;
    }
    public double getSide() {
        return side1;
    }
    public double perimeter() {
        return side1 + side2 + side3;
    }
    public double area() {
        return 0; //my mind go blank to calculate area for both triangle and tetrahedron
    }
}

class Tetrahedron extends ThreeDimensionalShape {
    Triangle tr;
    public Tetrahedron(double s1) {
        this.tr = new Triangle(s1,s1,s1,0);
    }
    public double area() {
        return 0;
    }
    public double volume() {
        return Math.sqrt(2.0)*tr.getSide()*tr.getSide()*tr.getSide()/12.0;
    }
}

class Tester {
    
    public static void main(String[] args) {
        Circle myCircle = new Circle(5);
        System.out.println("Circle: " + myCircle.area() + " " + myCircle.perimeter());
        Sphere mySphere = new Sphere(5);
        System.out.println("Sphere: " + mySphere.area() + " " + mySphere.volume());
        Square mySq = new Square(4);
        System.out.println("Square: " + mySq.area() + " " + mySq.perimeter());
        Cube myCube = new Cube(4);
        System.out.println("Cube: " + myCube.area() + " " + myCube.volume());
        Triangle myTr = new Triangle(3,4,5,4.7);
        System.out.println("Triangle: " + myTr.perimeter());
        Tetrahedron myTetra = new Tetrahedron(7);
        System.out.println("Tetrahedron: " + myTetra.volume());
    }
}
